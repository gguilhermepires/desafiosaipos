const express = require('express');
const controller = require('../controller/todo');
const router = express.Router();

router.post('/todo/news',controller.addNews);

router.post('/todo/auth',controller.todoAuth);

router.post('/todos',controller.addOne);

router.get('/todos',controller.getAll);

router.put('/todos/:id/status',controller.updateStatus);

module.exports = router;