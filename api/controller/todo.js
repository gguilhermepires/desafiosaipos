const Todo = require('../domain/todo');

exports.addOne = async (req, res, next) =>  {
    try{
        const todo = new Todo();
        const response = await todo.registerOneTodo(req.body)
        res.json(response);
    }catch(erro){
        let { code, message} = erro;
        if(code)
            res.status(code).json({ code, message });
        else
            res.status(500).json({ code: 500, message });
    }
};

exports.addNews = async (req, res, next) =>  {
    try{
        const todo = new Todo();
        const response = await todo.registerThreeTodo()
        res.json(response);
    }catch(erro){
        let { code, message} = erro;
        if(code)
            res.status(code).json({ code, message });
        else
            res.status(500).json({ code: 500, message });
    }
};

exports.todoAuth = async (req, res, next) =>  {
    const password = req.body.password;
    if(password == "TrabalheNaSaipos" || password == "123"){
        res.json({ code: 200, message: 'Sucesso' });
        return;
    }
    res.status(401).json({});
};

exports.getAll = async (req, res, next) =>  {
    try{
        const todo = new Todo();
        const response = await todo.getAllTodo(req.query)
        res.json(response);
    }catch(erro){
        let { code, message} = erro;
        if(code)
            res.status(code).json({ code, message });
        else
            res.status(500).json({code: 500, message });
    }
};

exports.updateStatus = async (req, res, next) =>  {
    try{
        const todo = new Todo();
        const todoId = parseInt(req.params.id);
        const response = await todo.updateTodoStatus(todoId, req.body)
        res.json(response);
    }catch(erro){
        let { code, message} = erro;
        if(code)
            res.status(code).json({ code, message });
        else
            res.status(500).json({ code: 500, message });
    }
};