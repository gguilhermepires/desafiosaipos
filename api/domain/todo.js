const ApiLayer = require('../services/apiLayer');
const ApiCat = require('../services/apiCat');
const TodoService = require('../services/todo');
const constants = require('../infrastructure/constants');

class Todo {

    async registerOneTodo(payload) {
        let todo = {}
        const email = payload.email;
        
        let response = await ApiLayer.validateEmail(email);
     
        if(response.status != 200)  
            return { code: 500, message: `Não foi possível comunicar com a ApiLayer` }; 
        
        let didMean = response.data.did_you_mean ? `Você quis dizer ${response.data.did_you_mean}` : '';

        if(response.data.mx_found == false)
            return { code: 400, message: `E-mail inválido. Erro: mx_found is false. ${didMean}` }; 
        
        if(response.data.format_valid == false)
            return { code: 400, message: `E-mail inválido. format_valid invalid.  ${didMean} ?` }; 
      
        if(response.data.success == false )
            return { code: 400, message: `E-mail inválido. Erro: ${response.data.error.type}. ${didMean}` }; 

        todo = {
            name: payload.name,
            email: email,   
            status: payload.status ? payload.status : constants.TODO_STATUS_PENDING,   
            description:payload.description,
            movement:payload.movement ? payload.movement: 0 
        }
        
        await TodoService.createOneTodo(todo)
        return { code: 200, message: 'Tarefa cadastrada com sucesso' };
    }

    async registerThreeTodo() {
        try{
            let description = '';
            let todo = {};
            let quantityTodoCreate = 0;

            for  (let i = 0; i < 3; i++)  {
              
                description = await ApiCat.getOneDogFact();
             
                todo = {
                    name: 'Eu',
                    email: 'eu@me.com',   
                    status: constants.TODO_STATUS_PENDING,   
                    description: description,
                    movement: 0
                }

                await TodoService.createOneTodo(todo);
                quantityTodoCreate += 1;
            }
            if(quantityTodoCreate < 3)
                throw new Error("Não foi possível criar 3 tarefas");

           return { code: 200, message: 'Tarefas criadas com sucesso' };
        }catch(erro){
            let { code, message } = erro;
            return { code: code ? code: 500, message };
        }
    }

    async getAllTodo(payload){
        try{
            let { status } = payload;
            let where ='';
            let order = [['id', 'DESC']];
            if(status)
                where = {status: status == undefined ? '' : status }
            return await TodoService.getAll(where,order);
        }catch(erro){
            let { code, message} = erro;
            return { code: code ? code:500, message };
        }
    }

    async updateTodoStatus(todoId, payload){
        try{
            let status = payload.status;
            
            let todo = await TodoService.getOneTodo(todoId);
            
            if(todo == null)
                return { code: 500, message: 'Não foi possível encontrar tarefa no banco de dados' };
          
            if(status == constants.TODO_STATUS_PENDING && todo.status == constants.TODO_STATUS_COMPLETE){
                if(todo.movement == 2)
                    return { code: 400, message: 'Não é possível mover a tarefa de completa para pendente mais de suas vezes' };
                todo.movement += 1;
            }
          
            todo.status = status;
            TodoService.updateOneTodo(todo);
            return { code: 200, message: 'Atualizado com sucesso'};
        }catch(erro){
            let { code, message } = erro;
            return { code: code ? code: 500, message };
        }
    }

  }
  module.exports = Todo;