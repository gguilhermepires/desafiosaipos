const Sequelize = require('sequelize');
const sequelize = require('../db/database').sequelize;

const Todo = sequelize.define('todo', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: Sequelize.STRING,
    email: Sequelize.STRING,   
    status: Sequelize.STRING,   
    description:Sequelize.STRING, 
    movement:Sequelize.INTEGER
});

module.exports = Todo;