const path = require('path');
const express = require('express');
const sequelize = require('./db/database').sequelize;
const cors = require('cors');
const todoRoute = require('./routes/todo');
require('dotenv').config();

const port = process.env.SERVER_PORT ? process.env.SERVER_PORT : 3000;
const app = express();
const server = require('http').createServer(app);

app.use(express.urlencoded({ extended: true}))
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});
app.use(cors({
    origin: 'http://localhost'
}));

app.use(todoRoute);
server.listen(port);
console.log("\n\n Servidor iniciado na porta:" + port);

sequelize.sync({ force: false })
.then(result => {
    console.log("Banco atualizado");
}).catch(e => {
    console.log("Erro banco:", e.toString());
});
