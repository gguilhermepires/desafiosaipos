module.exports = Object.freeze({
    TODO_STATUS_COMPLETE: 'COMPLETA',
    TODO_STATUS_PENDING: 'PENDENTE'
});