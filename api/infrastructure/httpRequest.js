const axios = require('axios');

class HttpRequest {
    static get(url){
        return axios.get(url)
    }
}

module.exports = HttpRequest;