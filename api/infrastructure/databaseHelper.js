class DatabaseHelper {
    
    static create(modelDatabase, data){
         modelDatabase.create(data);
    }

    static getAll(modelDatabase, _where='', _order=''){
        if(_where){
            if(_order)
                return modelDatabase.findAll({ where:_where, order:_order });
            return modelDatabase.findAll({ where:_where });
        }
        
        if(_order)
            return modelDatabase.findAll({ order:_order });
        return modelDatabase.findAll();
    }

    static getByPk(modelDatabase, pk){
        return modelDatabase.findByPk(pk);
    }

    static update(objectDataBase){
        objectDataBase.save();
    }
}
module.exports = DatabaseHelper;