const httpRequest = require('../infrastructure/httpRequest');

class ApiCat {

    static async getOneDogFact(){
        const url = `https://cat-fact.herokuapp.com/facts/random?animal_type=dog&amount=1`
        const response = await httpRequest.get(url);
        if(response.status == 200)
            return response.data.text;
        return '';
    }
}
module.exports = ApiCat;