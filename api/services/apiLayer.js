const httpRequest = require('../infrastructure/httpRequest');

class ApiLayer {
    static async validateEmail(email){
        const token = 'e7299be25aa8a88b9195ec8cd754a886';
        const url = `http://apilayer.net/api/check?access_key=${token}&email=${email}`
        return await httpRequest.get(url)
    }
}
module.exports = ApiLayer;