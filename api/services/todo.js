const TodoModel = require('../model/todo');
const DatabaseHelper = require('../infrastructure/databaseHelper');

class Todo {

    static async createOneTodo(todo){
        await DatabaseHelper.create(TodoModel, todo);
    }

    static async getAll(where='', order=''){
        return await DatabaseHelper.getAll(TodoModel, where, order);
    }
    static async getOneTodo(pk){
        return await DatabaseHelper.getByPk(TodoModel, pk);
    }
    static async updateOneTodo(todo){
        await DatabaseHelper.update(todo);
    }

}
module.exports = Todo;