# Desafio Saipos

Oba! Que ótimo poder contar com você no processo seletivo da Saipos! Buscamos pessoas
sensacionais para crescerem conosco e acreditamos que você pode ser uma delas.
Deixa a gente te explicar um pouco mais, ok? :)
Durante essas nossas primeiras interações, do lado de cá vamos construindo uma percepção
sobre você. Parte dela é subjetiva e embasada em nossas conversas, e parte é objetiva e leva
em consideração o seu desempenho em nosso code challenge, por isso é muito importante
que leia atentamente as instruções e lembre que quanto mais se dedicar aos detalhes, muito
provavelmente melhor tende a ser avaliado ao fim do desafio. Beleza?

## INSTRUÇÕES

Para o teste a seguir utilize a linguagem de sua preferência e que possui mais domínio, mas
lembre-se que aqui na Saipos utilizamos muito NodeJS e AngularJS, e você terá muito contato
com essas tecnologias.

O propósito é que você desenvolva um To-Do List (Lista de tarefas) simples, tanto front-end
quanto back-end e com um banco de dados relacional para armazenamento das informações.
Importante: não é necessário ter login para utilizar o ToDo.
Ao acessar, o usuário deverá ter acesso a lista de tarefas pendentes e também poderá
consultar a lista de tarefas já concluídas. Para incluir uma nova tarefa, ele deverá informar a
descrição, o nome do responsável pela mesma e o e-mail.

Apenas e-mails válidos poderão ser incluídos e para verificar isso deverá ser utilizada a API do
MailboxLayer (https://mailboxlayer.com/documentation - API KEY abaixo). Será considerado
um e-mail correto aquele que tem formato válido e que tenha os registros MX de acordo
também. Se o e-mail for inválido deverá sugerir ao usuário o endereço indicado pela API
(atributo did_you_mean).

Nessa tela será possível marcar tarefas pendentes como concluídas, movendo a tarefa para a
sessão de tarefas já realizadas. Tarefas já sinalizadas como concluídas poderão ser colocadas
como pendentes novamente, porém para isso o usuário deverá informar uma senha de
autorização, ou seja, apenas se um "supervisor" liberar é que ele poderá retornar a tarefa
como pendente. A senha de autorização é "TrabalheNaSaipos".

Uma tarefa poderá ser alterada de concluída para pendente apenas duas vezes, independente
da autorização do "supervisor".

Deverá também ter um botão "Estou sem tarefas", que precisará incluir três tarefas para o
usuário passar o tempo ocioso.

Essas tarefas deverão ser três fatos randômicos sobre Cachorros consultados na API
(https://alexwohlbruck.github.io/cat-facts/). Cada vez que o usuário clicar três novas tarefas
para ler e passar o tempo serão incluídas. Para esses casos o nome do responsável pode ser
"Eu" e o email "eu@me.com"._

MailboxLayer Key = f20f7ae318c34b92ee6a685fac758feb

Ao concluir o desafio, pedimos que o coloque em algum repositório como o Github e nos

sinalize. O prazo para conclusão é de 72 horas seguidas, ok?

Ficamos na torcida! Boa sorte!

# Solução

Utilizei NodeJS no back e AngularJS no front.

## Para executar api
1) Altere o .env para a configuração da sua máquina
2) Entre na pasta api e execute os comandos
npm install
npm start

## Para executar o front
1) coloque a pasta front no apache
2) abra a página index.html no navegador utilizando http