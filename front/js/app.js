var todoApp = angular.module('todoApp',[]);


todoApp.controller('TodoListController',['$scope','$http', function($scope, $http){
  var todoList = this;
  todoList.filter = '';
  todoList.host = 'http://localhost:3000';

  todoList.message = function(title){
    Swal.fire({
      title,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }
  
  todoList.AddNews = function() {
    $http({
      method: 'POST',
      url: `${todoList.host}/todo/news`,
      data:{}
  }).then(function successCallback(response) {
      todoList.message(response.data.message);
      todoList.loadList();
    }, function errorCallback(response) {
      todoList.message(response.data.message);
    });
  }

  todoList.loadList = function() {
    $http({
      method: 'GET',
      url: `${todoList.host}/todos?status=${todoList.filter}`,
  }).then(function successCallback(response) {
    todoList.todos = response.data;
    }, function errorCallback(response) {
      todoList.message('Ops! Aconteceu um erro ao carregar a lista de tarefas');
    });
  };
  
  todoList.addTodo = function() {
  
    const todo = {
      name: todoList.todoName,
      description: todoList.todoDescription,
      email: todoList.todoEmail
    }

    $http({
      method: 'POST',
      url: `${todoList.host}/todos`,
      data:todo
  }).then(function successCallback(response) {
      todoList.message(response.data.message);
      todoList.loadList();
    }, function errorCallback(response) {
      todoList.message(response.data.message);
    });
  };

  todoList.changeFilter = function(filter) {
    todoList.filter = filter;
    todoList.loadList();
  };

  todoList.todoUpdate = function(todo){
    $http({
      method: 'PUT',
      url: `${todoList.host}/todos/${todo.id}/status`,
      data: todo
  }).then(function successCallback(response) {

    if(response.status !=200){
      todoList.message('Ops! Aconteceu um erro ao atualizar a tarefa');
      return;
    }
   
    todoList.message(response.data.message);

    if(response.data.code != 200){
      return;
    }
    todoList.loadList();
    }, function errorCallback(response) {
      todoList.message('Ops! Aconteceu um erro ao atualizar a tarefa');
  });
  }
  todoList.changeStatus = function(todo, statusTarget) {
    if(todo.status == "COMPLETA" && statusTarget == "PENDENTE"){
      Swal.fire({
        title: 'Insira a senha de administrador',
        input: 'text',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Validar',
        showLoaderOnConfirm: true,
        preConfirm: (password) => {
          return fetch(`${todoList.host}/todo/auth`,
          {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({password})
          })
            .then(response => {
              if (response.status == 401) 
                throw new Error("Senha incorreta.")
              return response.json()
            })
            .catch(error => {
              Swal.showValidationMessage(
                error
              )
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.isConfirmed) {
          todoList.todoUpdate({...todo, status:statusTarget});
          todoList.loadList();
        }
      })
      return;
    }

    if(todo.status == "PENDENTE" && statusTarget == "COMPLETA"){
      todoList.todoUpdate({...todo, status:statusTarget});
    }
  };
  
  todoList.loadList();
}]);